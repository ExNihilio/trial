module.exports = {
	parser:'postcss-scss',
	plugins:{
		'postcss-assets': {cachebuster: true},
		autoprefixer: {},
		cssnano: {zindex: false, reduceIdents: false}
	}
};