import { ajax } from 'rxjs/ajax';
import { pluck, map } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';

export class Service{
    public getData(start: Date, end: Date, sector: number = 1, resolution): any{
        return ajax.get(
            'http://157.245.27.249:8080/api/data/sector' + sector + '?startDate=' + start.getTime() + '&endDate=' +
            end.getTime() + '&resolution=' + resolution + '&variableNames=value0,value1,value2'
        ).pipe(
            pluck('response', 'data', 'values'),
            map(x => Object.values(x))
        )
    }

    public getAllSectors(start, end, resolution): Observable<any>{
        return forkJoin(
            this.getData(start, end, 1, resolution),
            this.getData(start, end, 2, resolution),
            this.getData(start, end, 3, resolution)
        );
    }
}