import React from 'react';
import ReactDOM from 'react-dom';

import Header from '@component/header/header.component';
import Sidebar from '@component/sidebar/sidebar.component';
import Content from '@component/content/content.component';

import '@style/base';

const app = (
    <div className='container'>
        <Sidebar/>
        <div className='container__right'>
            <Header/>
            <Content/>
        </div>
    </div>
);

ReactDOM.render(app, document.getElementById('root'));