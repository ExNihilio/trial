import React, { Component } from 'react';
import { Route, HashRouter } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, ResponsiveContainer } from 'recharts';
import { useLocation } from 'react-router-dom';

import Overview from '@component/overview/overview.component';
import { Service } from '@service/service';

import '@component/content/content.style';
import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';

interface IContentState{
    startDate: Date | null;
    endDate: Date | null;
    data: {string: IData[]} | {} | null;
    index: number;
    resolution: number;
}

export interface IData{
    name: string;
    av: number;
    bv: number;
    cv: number;
}

export default class Content extends Component<Readonly<{}>, IContentState>{
    service: Service;

    constructor(props: Readonly<{}>){
        super(props);
        this.service = new Service();
        const startDate: Date = new Date();
        const endDate: Date = new Date(new Date().setHours(startDate.getHours() + 1));
        this.state = {
            startDate,
            endDate,
            data: null,
            index: this.getUrlIndex(),
            resolution: 10
        };
    }

    checkInputs(){
        if(this.state.startDate && this.state.endDate){
            this.service.getAllSectors(this.state.startDate, this.state.endDate, this.state.resolution).subscribe(
                success => this.setState({data: this.parseData(success)}),
                error => alert(error)
            );
        }
    }

    componentDidMount(){
        this.service.getAllSectors(this.state.startDate, this.state.endDate, this.state.resolution).subscribe(
            success => this.setState({data: this.parseData(success)}),
            error => alert(error)
        );

        fromEvent(window, 'hashchange')
            .pipe(map(event => (event as HashChangeEvent).newURL))
            .subscribe(url => {
                this.setState({ index: this.getUrlIndex(url) })
            });
    }

    getUrlIndex(url: string | null = null): number{
        url = url || window.location.hash;
        const urlMatches: RegExpMatchArray | null = url.match(/.+(\d)$/);
        if(urlMatches) return (urlMatches[1] as unknown as number) - 1;
        return -1;
    }

    parseData(data){
        const parsed = {};
        let index = 0;
        // tslint:disable-next-line: prefer-for-of
        for(let i = 0; i < data.length; i++){
            parsed[i] = [];
            for(let j = 0; j < data[i].length; j++){
                index = 0;
                for(const key of Object.keys(data[i][j])){
                    if(j === 0) parsed[i + ''].push({name: key, av: data[i][j][key]});
                    else if(j === 1) parsed[i + ''][index].bv = data[i][j][key];
                    else parsed[i + ''][index].cv = data[i][j][key];
                    index++;
                }
            }
        }

        return parsed;
    }

    setResolution = event => {
        let value: any = event.target.value.replace(/\D/g, '');
        if(value === '') value = 10;
        this.setState({
            resolution: value
        }, this.checkInputs)
    }

    setStartDate = date => {
        this.setState({
            startDate: date
        }, this.checkInputs);
    }

    setEndDate = date => {
        this.setState({
            endDate: date
        }, this.checkInputs);
    }

    public render(){
        return(
            <div>
                <div className='toolbox'>
                    <div>
                        <DatePicker
                        showTimeSelect
                        timeFormat='HH:mm'
                        timeIntervals={15}
                        timeCaption='time'
                        dateFormat='MMMM d, yyyy h:mm aa'
                        selected={this.state.startDate}
                        onChange={this.setStartDate}
                        maxDate={this.state.endDate}
                        placeholderText='Please select a date'
                        />
                    </div>
                    <div>
                        <DatePicker
                        showTimeSelect
                        timeFormat='HH:mm'
                        timeIntervals={15}
                        timeCaption='time'
                        dateFormat='MMMM d, yyyy h:mm aa'
                        selected={this.state.endDate}
                        onChange={this.setEndDate}
                        minDate={this.state.startDate}
                        placeholderText='Please select a date'
                        />
                    </div>
                    {this.state.index > 0 && <input type='text' value={this.state.resolution} onChange={this.setResolution}/>}
                </div>
                <HashRouter>
                    <Route path='/sectors/subsector([123])'>
                        {this.state.data &&
                        <ResponsiveContainer width='95%' height={400}>
                            <LineChart data={this.state.data[this.state.index]}>
                                <Line type='monotone' dataKey='av' stroke='#ffeb00'/>
                                <Line type='monotone' dataKey='bv' stroke='#10ff00'/>
                                <Line type='monotone' dataKey='cv' stroke='#c000ff'/>
                                <CartesianGrid stroke='#ccc' />
                                <YAxis />
                            </LineChart>
                        </ResponsiveContainer>}
                    </Route>
                </HashRouter>
                {this.state.data && <Overview index={this.state.index} sectorValues={this.state.data}/>}
            </div>
        );
    }
}
