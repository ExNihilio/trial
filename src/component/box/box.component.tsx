import React, { Component } from 'react';

import '@component/box/box.style';

interface IBoxProps{
    title: string;
    mean: number;
    median: number;
    min: number;
    max: number;
}

export default class Box extends Component<IBoxProps>{
    constructor(props: IBoxProps){
        super(props);
    }

    public render(){
        return(
            <div className='box'>
                <p className='box__title'>{this.props.title}</p>
                <div className='box__content'>
                    <p className='box__data'>
                        <span>Mean</span>
                        <span>{this.props.mean}</span>
                    </p>
                    <p className='box__data'>
                        <span>Median</span>
                        <span>{this.props.median}</span>
                    </p>
                    <p className='box__data'>
                        <span>Min</span>
                        <span>{this.props.min}</span>
                    </p>
                    <p className='box__data'>
                        <span>Max</span>
                        <span>{this.props.max}</span>
                    </p>
                </div>
            </div>
        );
    }
}
