import React, { Component } from 'react';

import 'react-datepicker/dist/react-datepicker.css';
import '@component/overview/overview.style';
import { IData } from '@component/content/content.component';
import Box from '@component/box/box.component';

interface IOverviewProps{
    sectorValues: {string: IData[]} | {} | null;
    index: number;
}

interface IOverviewState{
    sectorData: IMedianData[];
    detailData: IMedianData[];
    index: number;
}

interface IMedianData{
    min: number;
    max: number;
    mean: number;
    median: number;
    type: string;
    key: string;
}

export default class Overview extends Component<IOverviewProps, IOverviewState>{
    constructor(props: IOverviewProps){
        super(props);
        this.state = {
            sectorData: [{ min: 0, max: 0, mean: 0, median: 0, type: '', key: '0' }],
            detailData: [{ min: -1, max: -1, mean: -1, median: -1, type: 'Value 0', key: '0' }],
            index: -1
        }
    }

    static getDerivedStateFromProps(props, state){
        const l = props.sectorValues[0].length * Object.keys(props.sectorValues).length;
        const allSectorValues: number[][] = [];
        const currentSectorValues: number[][] = [[], [], []];
        const sectorMeans: number[] = [];
        const currentMeans: number[] = [0, 0, 0];
        const sectorObj: IMedianData[] = [];
        const detailObj: IMedianData[] = [];
        let allValues: number[] = [];
        for(const key of Object.keys(props.sectorValues)){
            allSectorValues[key] = [];
            sectorMeans[key] = 0;
            for (const item of props.sectorValues[key]){
                allSectorValues[key].push(item.av, item.bv, item.cv);
                sectorMeans[key] += item.av + item.bv + item.cv;
                if(key === '' + props.index){
                    currentSectorValues[0].push(item.av);
                    currentSectorValues[1].push(item.bv);
                    currentSectorValues[2].push(item.cv);
                    currentMeans[0] += item.av;
                    currentMeans[1] += item.bv;
                    currentMeans[2] += item.cv;
                }
            }
            allValues = [...allSectorValues[key]];
            allValues.sort((a, b) => a - b);
            allSectorValues[key].sort((a, b) => a - b)
            sectorMeans[key] /= l;
            sectorObj.push({
                type: 'Sector ' + key,
                min: allSectorValues[key][0],
                max: allSectorValues[key][l - 1],
                mean: sectorMeans[key],
                median: allSectorValues[key][Math.floor(l / 2)],
                key
            })
        }

        // tslint:disable-next-line: prefer-for-of
        for(let i = 0; i < currentSectorValues.length; i++){
            currentMeans[i] /= currentSectorValues[i].length;
            currentSectorValues[i].sort((a, b) => a - b);
            detailObj.push({
                type: 'Value ' + i,
                min: currentSectorValues[i][0],
                max: currentSectorValues[i][currentSectorValues[i].length - 1],
                mean: currentMeans[i],
                median: currentSectorValues[i][Math.floor(currentSectorValues[i].length / 2)],
                key: i + ''
            });
        }

        sectorObj.push({
            type: 'Overall',
            min: allValues[0],
            max: allValues[allValues.length - 1],
            mean: sectorMeans.reduce((acc, x) => acc + x) / 3,
            median: allValues[Math.floor(allValues.length / 2)],
            key: '9999'
        });

        return {
            sectorData: sectorObj,
            detailData: detailObj,
            index: props.index
        }
    }

    public render(){
        return(
            <div className='overview'>
                {(this.state.index >= 0 ? this.state.detailData : this.state.sectorData).map(x => {
                    return <Box key={x.key} title={x.type} mean={x.mean} median={x.median} min={x.min} max={x.max}/>;
                })}
            </div>
        );
    }
}
