import React, { Component } from 'react';
import { NavLink, HashRouter } from 'react-router-dom';

import '@component/sidebar/sidebar.style';

export default class Sidebar extends Component{
    constructor(props: Readonly<{}>){
        super(props);
    }

    public render(){
        return(
            <div className='sidebar'>
                <div>
                    <figure className='sidebar__logo'>
                        <img src='./assets/logo.png' alt='logo'/>
                    </figure>
                </div>
                <div>
                    <HashRouter>
                        <NavLink to='/overview' className='sidebar__link' activeClassName='sidebar__link--active'>Overview</NavLink>
                        <NavLink to='/sectors/subsector1' className='sidebar__link'>Sectors</NavLink>
                        <div className='sidebar__submenu'>
                            <NavLink exact={false} to='/sectors/subsector1' className='sidebar__link' activeClassName='sidebar__link--active'>Sector 1</NavLink>
                            <NavLink to='/sectors/subsector2' className='sidebar__link' activeClassName='sidebar__link--active'>Sector 2</NavLink>
                            <NavLink to='/sectors/subsector3' className='sidebar__link' activeClassName='sidebar__link--active'>Sector 3</NavLink>
                        </div>
                    </HashRouter>
                </div>
            </div>
        );
    }
}
