const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    entry: {
        main: './src/index.tsx'
    },
    module: {
        rules: [
            {test: /\.tsx?$/, exclude: /node_modules/, use: ['ts-loader']},
            {test: /\.html$/, exclude: /node_modules/, use: ['html-loader']},
            {test: /\.css$/, use: [
                'style-loader', 'css-loader'
            ]},
            {test: /\.scss$/, exclude: /node_modules/, use: [
                'style-loader', 'css-loader', 'postcss-loader', 'sass-loader'
            ]}
        ]
    },
    resolve: {
        alias: {
            '@asset': path.resolve(__dirname, 'src/asset/'),
            '@component': path.resolve(__dirname, 'src/component/'),
            '@service': path.resolve(__dirname, 'src/service/'),
            '@style': path.resolve(__dirname, 'src/style/')
        },
        extensions: ['.ts', '.js', '.css', '.scss', '.html', '.webpack.js', '.web.js', '.tsx', '.jsx', '.json']
    },
    plugins: [
        new HtmlWebpackPlugin({
            hash: true,
            template: './src/index.html',
            filename: 'index.html'
        })
    ],
};